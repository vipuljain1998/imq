class Protocol:
    def __init__(self, data, source, destination):
        self.data = data
        self.source = source
        self.destination = destination

    def get_data(self):
        return self.data

    def get_source(self):
        return self.source

    def get_destination(self):
        return self.destination
