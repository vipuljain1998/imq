import datetime
from constants import *

class Message:
    def __init__(self):
        pass

    def create_message(self, message):
        msg_dict = {
            "msg": message,
            "createdAt": str(datetime.datetime.now()),
            "expiredAt": str(datetime.datetime.now()+ datetime.timedelta(days=4))
        }
        return msg_dict
