import socket
import unittest
from settings import *
from client.client_class import Client
from constants import *

class ClientTest(unittest.TestCase):
    def test_sockets(self):
        client = Client()
        client_socket = client.create_socket()
        client_socket.close()
        test_socket = socket.socket()
        test_socket.close()
        self.assertEqual(type(client_socket), type(test_socket))

    def test_imq_protocol(self):
        client = Client()
        imq_protocol = client.get_imq_protocol('Testing', '127.0.0.1:8787', '127.0.0.1:9898')
        self.assertEqual('127.0.0.1:8787', imq_protocol.source)


if __name__ == '__main__':
    unittest.main()
