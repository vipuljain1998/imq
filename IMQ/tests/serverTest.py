import unittest
from settings import *
from dbLayer.database import Database
from protocols.protocol import Protocol
from server.server_class import Server
from constants import *

class ServerTest(unittest.TestCase):
    def test_db_object(self):
        server = Server()
        db_obj = server.get_db_object()
        db_test = Database()
        self.assertEqual(type(db_test), type(db_obj))

    def test_imq_protocol(self):
        server = Server()
        imq_protocol = server.get_imq_protocol('testing', '127.0.0.1:8787', '127.0.0.1:9898')
        imq_protocol_obj = Protocol('testing', '127.0.0.1:8787', '127.0.0.1:9898')
        self.assertEqual(type(imq_protocol), type(imq_protocol_obj))


if __name__ == '__main__':
    unittest.main()
