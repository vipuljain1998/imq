import os
from datetime import datetime
from constants import *

class Loggers:
    def __init__(self):
        pass

    def create_log_file(self, file_name, message):
        root_dir = os.path.dirname(os.path.abspath(__file__))
        file_pointer = open(root_dir + "/" + file_name, "a")
        timestamp = datetime.now().strftime("%H:%M:%S")
        file_pointer.write(str(timestamp) + " " + message + "\n")
        file_pointer.close()


