import pickle
import socket
from settings import *
from dbLayer.database import Database
from exceptions.server import ServerException
from protocols.protocol import Protocol
from server.services.authentication import Authentication
from server.queueHandler import Queue
from server.services.register import Register
from loggers.loggers import Loggers
from constants import *



class Server:
    def __init__(self):
        pass

    def create_socket(self):
        server_socket = socket.socket()
        return server_socket

    def bind_socket(self, server_socket):
        try:
            server_socket.bind((HOST, PORT))
        except ServerException as e:
            e.GetErrorMessage()

    def get_logger_object(self):
        log = Loggers()
        return log

    def get_db_object(self):
        db = Database()
        return db

    def get_imq_protocol(self, data, source, destination):
        return Protocol(data, source, destination)

    def recieve_message(self, conn):
        data = conn.recv(1024)
        imq_protocol = pickle.loads(data)
        return imq_protocol.get_data()

    def send_message(self, conn, imq_protocol):
        conn.send(pickle.dumps(imq_protocol))

    def get_topics(self):
        db = self.get_db_object()
        db_conn = db.make_connection()
        cursor = db.get_cursor(db_conn)
        return db.get_topics(cursor)

    def register_imq_topic(self, topic_name, client_id):
        db = self.get_db_object()
        db_conn = db.make_connection()
        cursor = db.get_cursor(db_conn)
        response = db.register_imq_topic(cursor, topic_name, client_id)
        db_conn.commit()
        db.close_connection(db_conn)
        return response

    def get_registered_topics(self, client_id):
        db = self.get_db_object()
        db_conn = db.make_connection()
        cursor = db.get_cursor(db_conn)
        response = db.get_registered_topics(cursor, client_id)
        return response

    def push_message(self, client_id, topic_name, message):
        db = self.get_db_object()
        db_conn = db.make_connection()
        cursor = db.get_cursor(db_conn)
        response = db.push_message(cursor, topic_name, client_id, message)
        db_conn.commit()
        db.close_connection(db_conn)
        return response

    def pull_message(self, client_id, topic_name):
        db = self.get_db_object()
        db_conn = db.make_connection()
        cursor = db.get_cursor(db_conn)
        response = db.pull_message(cursor, client_id, topic_name)
        if isinstance(response, str):
            return response
        else:
            queue = Queue(response)
            self.update_message_status(client_id, topic_name, queue.get_data())
            return response

    def update_message_status(self, client_id, topic_name, data):
        db = self.get_db_object()
        db_conn = db.make_connection()
        cursor = db.get_cursor(db_conn)
        db.set_status_dead(client_id, topic_name, data, cursor)
        db_conn.commit()

    def HandleClient(self, conn, addr):
        print(f"New Connection {addr} pinged")
        imq_protocol = self.get_imq_protocol('Server is working', 'localhostPort',
                                           str(addr[0]) + ':' + str(addr[1]))
        self.send_message(conn, imq_protocol)
        connected = True
        client_id = None
        response = ""
        while connected:
            msg = self.recieve_message(conn)
            data = msg
            log = self.get_logger_object()
            file_name = str(addr[0]) + "_" + str(addr[1]) + ".txt"
            log.create_log_file(file_name, msg)
            if msg.split('@')[0] == 'R':
                id = msg.split('@')[1]
                password = msg.split('@')[2]
                confirm_password = msg.split('@')[3]
                authentication = Register()
                if authentication.reconfirm_password(password, confirm_password):
                    authentication.register_client(id, password)
                    response = 'Registartion Successfully'
                else:
                    response = 'Password and Confirm Password should match'
            elif msg.split('@')[0] == 'L':
                id = msg.split('@')[1]
                password = msg.split('@')[2]
                authentication = Authentication()
                if authentication.login(id, password):
                    response = 'login Successfully'
                    client_id = id
                else:
                    response = 'login Failed'
            elif msg == 'S':
                print("Logout Successfully")
                conn.close()
                exit()
            elif msg == 'T':
                response = self.get_topics()
            elif msg.split('@')[0] == 'Register':
                topic_name = msg.split('@')[1]
                response = self.register_imq_topic(topic_name, client_id)
            elif msg == 'GT':
                response = self.get_registered_topics(client_id)
            elif msg.split('@')[0] == 'Pub':
                topic_name = msg.split('@')[2]
                message = msg.split('@')[1]
                response = self.push_message(client_id, topic_name, message)
            elif msg.split('@')[0] == 'Sub':
                topic_name = msg.split('@')[1]
                response = self.pull_message(client_id, topic_name)
            else:
                print(f"{addr} {msg}")
                response = data
                db = self.get_db_object()
                db_conn = db.make_connection()
                cursor = db.get_cursor(db_conn)
                if not (db.is_client_table_exist(cursor, client_id)):
                    db.create_client_table(cursor, client_id)
                    db.make_enrolled_client_record(cursor, client_id)
                    db_conn.commit()
                db.make_client_record(cursor, client_id, data)
                db_conn.commit()
                db.close_connection(db_conn)
            imq_protocol.data = response
            self.send_message(conn, imq_protocol)
        conn.close()
