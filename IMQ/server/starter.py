import threading
from settings import *
from server.server_class import Server
from constants import *
if __name__ == "__main__":
    server = Server()
    print("Server is starting")
    socket = server.create_socket()
    server.bind_socket(socket)
    socket.listen(5)
    while True:
        connection, address = socket.accept()
        thread = threading.Thread(target=server.HandleClient, args=(connection, address))
        thread.start()
