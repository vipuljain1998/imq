from settings import *
from dbLayer.database import Database


class Authentication:
    def __init__(self):
        pass

    def get_db_object(self):
        db = Database()
        return db

    def login(self, id_, password_):
        db = self.get_db_object()
        db_conn = db.make_connection()
        cursor = db.get_cursor(db_conn)
        table_name = 'client'
        query = ("SELECT id, password FROM {} WHERE id = %s AND password = %s".format(table_name))
        try:
            id = id_
            password = password_
            print(id,password,type(id),type(password))
            data = (id, password)
            cursor.execute(query, data)
            result = cursor.fetchone()
            print(result[0],result[1],type(result[0]),type(result[1]))
            db.close_connection(db_conn)
            if result is None:
                return False
            if str(result[0]) == id and result[1] == password:
                return True
            else:
                return False
        except Exception as error:
            print(error)
