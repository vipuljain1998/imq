from settings import *
from dbLayer.database import Database


class Register:
    def __init__(self):
        pass

    def get_db_object(self):
        db = Database()
        return db

    def register_client(self, id, password):
        db = self.get_db_object()
        db_conn = db.make_connection()
        cursor = db.get_cursor(db_conn)
        table_name = 'client'
        query = ("INSERT INTO {} (id, password) VALUES(%s, %s)".format(table_name))
        try:
            user_id = id
            user_password = password
            data = (user_id, user_password)
            cursor.execute(query, data)
            db_conn.commit()
            db.close_connection(db_conn)
        except Exception as error:
            print(error)

    def reconfirm_password(self, password, confirm_Password):
        if password == confirm_Password:
            return True
        else:
            return False
