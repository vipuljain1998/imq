import json
import pickle
import socket
from settings import *
from constants import *
from entities.messageHandler import Message
from protocols.protocol import Protocol


class Client:
    def __init__(self):
        pass

    def create_socket(self):
        client_socket = socket.socket()
        return client_socket

    def connect_server(self, client_socket):
        try:
            client_socket.connect((HOST, PORT))
        except Exception as e:
            print(str(e))

    def send_message(self, client_socket, msg):
        client_socket.send(pickle.dumps(msg))

    def recieve_message(self, client_socket):
        return pickle.loads(client_socket.recv(1024))

    def get_imq_protocol(self, data, source, destination):
        return Protocol(data, source, destination)

    def display_menu(self):
        print("Enter R for register\n")
        print("Enter L for login\n")
        print("Enter S for SignOut\n")

    def send_recieve_message(self, client_socket, msg):
        self.send_message(client_socket, msg)
        imq_protocol = self.recieve_message(client_socket)
        message = imq_protocol.get_data()
        return message

    def display_client_menu(self):
        print("Enter T to show Topics\n")
        print("Enter Register to register the topic\n")
        print("Enter GT to get Registered Topics\n")
        print("Enter P to publish or consume message\n")
        print("Enter Return for back\n")

    def show_topic_menu(self, topic, client_socket, source, destination):
        print("Enter Pub to Publish Message\n")
        print("Enter Sub to Consume Message\n")
        print("Enter Return to get back")
        while True:
            request = input("Enter response Pub=Publish Sub=Consume Return=back\n")
            if request == 'Pub':
                data = input("Enter Message to publish\n")
                message_obj = Message()
                formatted_message = message_obj.create_message(data)
                message_to_server = request + '@' + json.dumps(formatted_message) + '@' + topic
                protocol = self.get_imq_protocol(message_to_server, source, destination)
                received_response = self.send_recieve_message(client_socket, protocol)
                print(received_response)
            elif request == 'Sub':
                requested_msg = request + '@' + topic
                protocol = self.get_imq_protocol(requested_msg, source, destination)
                received_response = self.send_recieve_message(client_socket, protocol)
                if isinstance(received_response, str):
                    print(received_response)
                else:
                    print("Message Is ...............\n")
                    print(received_response[0])
                    print('\n')
            elif request == 'Return':
                break
            else:
                print("Enter Valid options")
