from settings import *
from constants import *
from client.client_class import Client

if __name__ == "__main__":
    client = Client()
    socket = client.create_socket()
    client.connect_server(socket)
    client.display_menu()
    is_authenticated = False
    response_protocol = client.recieve_message(socket)
    res = response_protocol.get_data()
    source = response_protocol.get_source()
    destination = response_protocol.get_destination()

    while True:
        msg = input("Enter Message R=Register L=login S=SignOut\n")
        if msg == 'R':
            id = input('Enter Client Id:\n')
            password = input('Enter Password\n')
            confirm_password = input('Enter Password Again\n')
            msg = msg + "@" + id + '@' + password + '@' + confirm_password
        elif msg == 'L':
            id = input('Enter Client Id:\n')
            password = input('Enter Password\n')
            msg = msg + "@" + id + '@' + password
        elif msg == 'S':
            imq_protocol = client.get_imq_protocol(msg, source, destination)
            client.send_message(socket, imq_protocol)
            break
        else:
            if not is_authenticated:
                print('Please login or Register')
                client.display_menu()
                continue
        if msg != '':
            imq_protocol = client.get_imq_protocol(msg, source, destination)
            client.send_message(socket, imq_protocol)
            res = client.recieve_message(socket).get_data()
            print(res)
        if res == 'login Successfully':
            is_authenticated = True
            client.display_client_menu()
            while True:
                request_message = input("Enter your choice between T=Topics, Register, GT=RegisterTopics, "
                                        "P=Publish/Consume Return=back\n")
                if request_message == 'T':
                    imq_protocol = client.get_imq_protocol(request_message, source, destination)
                    response = client.send_recieve_message(socket, imq_protocol)
                    print("Topics ......\n")
                    for topic in response:
                        print(topic[0])
                    print('\n')
                elif request_message == 'Register':
                    message = input("Enter Topic Name to Register\n")
                    request_message = request_message + '@' + message
                    imq_protocol = client.get_imq_protocol(request_message, source, destination)
                    response = client.send_recieve_message(socket, imq_protocol)
                    print(response)
                    print("\n")

                elif request_message == 'GT':
                    imq_protocol = client.get_imq_protocol(request_message, source, destination)
                    registered_topics = client.send_recieve_message(socket, imq_protocol)
                    if isinstance(registered_topics, str):
                        print(registered_topics)
                    else:
                        print("Registered Topics ......\n")
                        for topic in registered_topics:
                            print(topic[0][0])
                    print('\n')
                elif request_message == 'P':
                    print("Choose from Registered Topics ......\n")
                    imq_protocol = client.get_imq_protocol('GT', source, destination)
                    registered_topics = client.send_recieve_message(socket, imq_protocol)
                    if isinstance(registered_topics, str):
                        print(registered_topics)
                    else:
                        print("Registered Topics ......\n")
                        for topic in registered_topics:
                            print(topic[0][0])
                        print('\n')
                        topic_name = input("Choose Topic Name from registered topics to publish or consume message\n")
                        client.show_topic_menu(topic_name, socket, source, destination)
                elif request_message == 'Return':
                    break

    socket.close()
